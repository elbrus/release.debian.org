#! TITLE: Buster Freeze Timeline and Policy
#! SUBTITLE: let's release!


The freeze for buster will happen according to the following timeline:

 * 2019-01-12 - Transition freeze
 * 2019-02-12 - Soft-freeze
 * 2019-03-12 - Full-freeze


Before the freeze
=================


Plan your changes for buster
----------------------------

If you are planning big or disruptive change, check the timeline to see if
it's still realistic to finish them before the transition freeze. Keep in mind
that you might need cooperation from other volunteers, who might not
necessarily have the time or capacity to work on this. You can stage your
changes in experimental to make sure all affected packages are ready before
uploading the changes to unstable.


Fix release-critical bugs
-------------------------

You can help the development of buster by fixing RC bugs, before and during
the freeze. An overview of bugs can be found on
<a href="https://udd.debian.org/bugs/">the UDD bugs page</a>.

You can also join the #debian-bugs irc channel on irc.oftc or join a
<a href="https://wiki.debian.org/BSP">Bug Squashing Party</a>.


Testing migration
-----------------

The 'standard' testing migration rules apply:

 * automatic migration after 2/5/10 days for priority high/medium/low packages
 * faster migration for packages with successful autopkgtests
 * delayed migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages
 * packages not in testing can migrate to testing
 * no manual review by the release team

Transition Freeze
=================

Starting 2019-01-12, **new transitions and large/disruptive changes** are no
longer acceptable for buster.

Autopkgtests regressions
------------------------

Starting 2019-01-12, packages which trigger autopkgtests failures in unstable,
which succeed in testing, will be blocked from migrating to testing. This
means autopkgtests regression will treated the same way as RC bug regressions
or piuparts regressions.

Testing migration
-----------------

Changes in the rules for testing migration:

 * no migration for packages that trigger autopkgtest regressions

Other than that, the 'standard' testing migration rules apply:

 * automatic migration after 2/5/10 days for priority high/medium/low packages
 * faster migration for packages with successful autopkgtests
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages
 * packages not in testing can migrate to testing
 * no manual review by the release team


Soft Freeze
===========

Starting 2019-02-12, only **small, targeted fixes** are appropriate for
buster. We want maintainers to focus on small, targeted fixes. This is
mainly, at the maintainers discretion, there will be no hard rule that will be
enforced.

Please note that new transitions or large/disruptive changes are not
appropriate.

The release team might block the migration to testing of certain changes if
they might cause disruption for the release process.

Increased delay for all testing migrations
------------------------------------------

The testing migration delay of all packages will be increased to 10 days. This
means the rate of changes to testing will slow down.

The minimum delay of 10 days will also apply to packages with successful
autopkgtests.


No new packages and no re-entry to testing
------------------------------------------

Packages that are not in testing will not be allowed to migrate to testing.
This applies to new packages as well as to packages that were removed from
testing (either manually or by auto-removals). Packages that are not in buster
at the start of the soft freeze will not be in the release.

Please note that packages that are in buster at the start of the soft freeze
can still be removed if they are buggy. This can happen manually or by the
auto-removals. Once packages are removed, they will not be allowed to come
back.


Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for some packages
 * migration delay always at least 10 days
 * no faster migration for packages with successful autopkgtests
 * packages not in testing can not migrate to testing

The following rules still apply:

 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages

Full freeze
===========

Starting 2019-03-12, packages can only migrate to testing after manual review
by the release team.

We have some criteria for what changes we are going to accept.  These
are listed below.  These criteria
will become more rigid as the freeze progresses.

The release managers may make exceptions to these guidelines as they
see fit. **Such exceptions are not precedents and you should not
assume that your package has a similar exception.** Please talk to us
if you need guidance.

Please talk to us early and do not leave issues to the last minute. We
are happy to advise in case you need the release team's help to fix RC
bugs (e.g.  to remove an old package)


Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for all packages, based on the criteria
   listed below

The following rules continue to apply:

 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages
 * packages not in testing can not migrate to testing

Changes which can be considered
-------------------------------

 1. targeted fixes for release critical bugs (i.e., bugs of severity critical, grave, and serious) in all packages;
 1. fixes for severity: important bugs in packages of priority: optional or extra, only when this can be done via unstable;
 1. translation updates and documentation fixes that are included with fixes for the above criteria;

Note that when considering a request for an unblock, the changes
between the (proposed) new version of the package in `unstable` and
the version currently in `testing` are taken in to account. If there
is already a delta between the package in `unstable` and `testing`,
the relevant changes are all of those between `testing` and the new
package, not just the incremental changes from the previous `unstable`
upload. This is also the case for changes that were already in
`unstable` at the time of the freeze, but didn't migrate at that
point.

We strongly prefer changes that can be done via unstable instead of
testing-proposed-updates. If there are unrelated changes in unstable, you
should consider reverting these instead of making an upload to
testing-proposed-updates.


Applying for an unblock
-----------------------

 1. Prepare a **source** `debdiff` between the version in `testing` and `unstable` and check it carefully
 1. Use `reportbug` to report an unblock bug against the `release.debian.org` meta-package. Attach the source diff. Include a detailed justification of the changes and references to bug numbers.
 1. If the diff is small and you believe it will be approved, you can upload it to unstable before filing the unblock request to avoid a round-trip.
 1. Depending on the queue, there may be some delay before you receive further instructions.

If you are unable to bring your fix through unstable, for example
because there are unrelated changes already uploaded there, the
release team can grant you permission to use the
`testing-proposed-updates` mechanism. Prepare an upload targeting
`testing-proposed-updates` but **do not upload it**, and then contact
us through an unblock bug. It is usually better to revert the changes in
unstable and upload the fix there.

Targeted fixes
--------------

A targeted fix is one with only the minimum necessary changes to
resolve a bug. The freeze process is designed to make as few changes
as possible to the forthcoming release. Uploading unrelated changes is
likely to result in a request for you to revert them if you want an
unblock.

Some examples of changes that are undesirable during a freeze:

 1. dropping a -dbg package in favour of -dbgsym
 1. adding a new systemd unit in place of an init script


Removing packages from testing during the freeze
================================================

Throughout the freeze, we will continue to remove non-key
packages with RC bugs in testing and their reverse dependencies automatically.
As usual, the auto-removal system will send a notice before this happens.
Please note that it is not enough for the bug to be fixed in unstable.
The fix (in unstable or testing-proposed-updates) must be done in such a way that
the fixed package can be unblocked and allowed to migrate to testing, based on the rules
listed above. **You must contact us to ensure the fix is unblocked - do not rely on
the release team to notice on their own.**

Manual removal may still happen without warning before the standard auto-removal
periods have passed, when the package is blocking other fixes.

After 12th February 2019, removed packages will **not** be permitted to re-enter testing.
