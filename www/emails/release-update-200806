Subject: Release Update: freeze, architecture requalification
To: debian-devel-announce@lists.debian.org

Hi *,

Normally, you'd have a pithy comment about how we're writing to you with
lots of info and things. However, due to the timeline, please accept the
small picture below:
                    ______________________
                   < We freeze next week! >
                    ----------------------
                            \   ^__^
                             \  (oo)\_______
                                (__)\       )\/\
                                    ||----w |
                                    ||     ||

Freeze status
~~~~~~~~~~~~~
Since the last update, the following steps in the timeline have been
taken:
  Freeze of the non-essential toolchain
    The "non-essential toolchain" means things like debhelper, cdbs
    and a big chunk of other things usually needed to produce binary
    packages.
  Freeze of all library packages
    This will affect all packages that produce library packages used
	by other packaged software. Packages without r-deps won't be
	frozen at this point.

I'll point out once again, that maintainers should look at the timeline
and realise that next week we *FREEZE*.


Architecture status
~~~~~~~~~~~~~~~~~~~
The architecture qualification pages on wiki.debian.org are still
missing a LOT of information.
Of the current 12 architectures, 8 are still at risk of being dropped
unless their issues can be solved. Check the [ARCH:QUAL] pages for more
information as to the current issues. In some cases, these arches are
not being considered because we may not have enough information in the
wiki pages to make an informed judgement.


Release goals
~~~~~~~~~~~~~
* Switch /bin/sh to dash

There are still quite a few bugs open about bashisms, but most of those
have a patch included. Please NMU. This goal doesn't mean we'd switch to
dash as a global default, but if people do so on their system, there
shouldn't be any issue after the last bugs have been finished off.

* piuparts-clean archive

Over 50 bugs remaining, many with little activity.  Since these are
problems that affect all users and are usually fixed by little changes to
the maintainer scripts, more attention to this goal would be very
welcome.

* double compilation support

Most of the double compilation problems have been fixed since the last
release update and there are only about three dozen bugs left. Please note
that many of the packages still affected are in bad general shape, so each
NMUer should consider if the package in question shouldn't be removed
instead.

* Prepare init.d-Scripts for dependency-based init systems

Wider testing of dependency-based init systems has lead to some new bugs
for this goal, but the current state looks quite well. We are confident
that we will have full support for dep-based init system in lenny.


BSP Marathon
~~~~~~~~~~~~
At time of writing, we have 360 open RC bugs affecting lenny, which is 
360 too many.  A coordinated effort is needed to reduce this number, so 
we've decided to resurrect last year's very successful BSP marathons. As 
a reminder, we still have a 0-day NMU policy in effect.

Please note that in a BSP, you shouldn't just NMU every RC bug you see.
While you are working on a package, check for other low-hanging fruits
(like translation updates, typos that can easily be fixed, ...) and fix
them in your NMU. On the other hand, if you notice that a package looks
unmaintained, refrain from fixing the bugs for now and try to find out if
the package should be removed or adopted by another maintainer instead.

In case you want to organise a BSP, don't hesitate to contact the Release
Team to coordinate efforts.


Release schedule
~~~~~~~~~~~~~~~~
If you've managed to miss my (ok, cowsay's) wonderful artwork, please
see the following:

Next week
  Full freeze
    Please don't wait with uploads for the last day before the freeze,
    thanks.

September 2008
  Release lenny!


Tricks from the Release Team
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Curious as to why your package isn't getting into testing? Have a look
at the [MIG] Migration Tracker page. Although if it's a library, or
you're reading this mail after next week, it's because we've frozen. Did I
mention that already?

-- 
http://release.debian.org
Debian Release Team

References:
 [ARCH:QUAL] http://release.debian.org/lenny/arch_qualify.html
 [RG:D] http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-dash
 [RG:P]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=piuparts-stable-upgrade&dist=testing
 [MIG] http://release.debian.org/migration/
