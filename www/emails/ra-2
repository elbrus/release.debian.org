Subject: T&S 2 - Die^H^H^HRelease Harder

Hi,

the first round of T&S for RAs is done.

First of all, thank you all for participating in this, and thanks for
squashing bugs. We seem to have already lost some participiants.

Survived until now have
Guido Trotter
Andrea Mennucci
Marc 'HE' Brockschmidt
Luk Claes
Bill Allombert
and also Adeodato Simó (on the vacations list)




This week, we go into more details.  As you can see from the delays in
getting new assignments to you, release team work requires a great deal
of self-direction -- our goal is to equip you with the skillset you need
in order to dig in to the list of RC issues on your own, and continue
working when we're not looking... of course, also helping out with areas 
identified as critical when help is needed.

So first, please continue to work on the RC bugs you already have, and
select at least two RC bugs from our RC bug list and add them to your
work list.

Second, in addition to fixing *reported* RC bugs, you will often find
that you need to find, report, *and* fix bugs in others' packages.  One
category of bugs where this may be the case is build failures.

Each of you will find below assigned to you a build failure from
buildd.debian.org.  Your task is to analyze/reproduce the build failure,
file any bugs necessary, and follow through until a fixed package
reaches testing.  You may of course consult the appropriate porters.
There may or may not be a bug filed already on the package, though most
such bugs don't offer much help in understanding the cause of the
failure anyway.

You should all be familiar already with the list of porter machines,
http://db.debian.org/machines.cgi, and know to contact debian-admin if
you need build-dependencies installed for a test build.


Marc 'HE' Brockschmidt
http://buildd.debian.org/fetch.php?pkg=twin&arch=mips&ver=0.5.1-3&stamp=1142723928&file=log

Andrea Mennucci
http://buildd.debian.org/fetch.php?pkg=db4.3&arch=hppa&ver=4.3.29-5&stamp=1148476663&file=log

Guido Trotter
http://buildd.debian.org/fetch.php?pkg=ecj-bootstrap&arch=arm&ver=3.1.2-5&stamp=1147731709&file=log

Adeodato Simó
http://buildd.debian.org/fetch.php?pkg=tagcoll&arch=hppa&ver=1.6.2-2&stamp=1148891155&file=log

Luk Claes
http://buildd.debian.org/fetch.php?pkg=subversion&arch=alpha&ver=1.3.1-3&stamp=1146945776&file=log

Bill Allombert
http://buildd.debian.org/fetch.php?pkg=kaffe&arch=ia64&ver=2%3A1.1.7-2&stamp=1146095882&file=log


Now, we'll show you something else: how to bring updates into testing where
more packages need to travel together. Take for example
      cdcd |  0.6.5-4.1 |       testing | source
libcdaudio | 0.99.9-2.1 |       testing | source
  audio-cd |     0.05-4 |       testing | source

Now, there are these updates waiting:
      cdcd |    0.6.6-2 |      unstable | source
libcdaudio | 0.99.12p2-1 |      unstable | source
  audio-cd |     0.05-6 |      unstable | source

However:
Package: cdcd
Version: 0.6.6-2
Depends: libc6 (>= 2.3.5-1), libcdaudio1 (>= 0.99.12p2-1), libncurses5 (>= 5.4-5), libreadline5 (>= 5.1)
but libcdaudio1 is only in unstable (as part of libcdaudio), whereas the
testing version of cdcd depends on libdaudio0 (which is provided from the
old version of libcdaudio). Now, libcdaudio cannot be updated because it
breaks cdcd, and cdcd cannot be updated, because it needs the new
libcdaudio. The solution is to bring both packages in together (and also
audio-cd, as that also depends on it).

Technically, we write:
easy cdcd/0.6.6-2 libcdaudio/0.99.12p2-1 audio-cd/0.05-6

That will bring in all three versions at the same time, and compares the
result after that if it's worse (regarding installability count) than it
used to be before, and if not, commits is.

Your last task for the next two weeks is now to find at least one set of
packages, e.g. on http://bjorn.haxx.se/debian/, and send us an working
hint. Please don't be disappointed if your hint doesn't work on first try -
it's not that easy, but as you're now a bit more experienced, you should
forward to that level.
