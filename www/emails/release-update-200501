DRAFT
DRAFT
DRAFT
DRAFT
DRAFT
DRAFT

To: debian-devel-announce@lists.debian.org
Subject: Release update: kde3.3, upload targets, kernels, infrastructure

Happy New Year to all of our loyal d-d-a subscribers!


RC bug count, BSP
-----------------

After the expected slide in activity and backslide in bug count over the
holidays, the release team has gotten an aggressive start on the new
year, starting off with the arrival of first a new gcc-3.3 (for some
ia64 fixes) and then KDE3.3 in testing, allowing us to clear a hefty
chunk of release-critical bugs (including numerous security bugs) from
the list for sarge.  This was followed almost immediately by an
upload to unstick perl, which has been plagued by autobuilder problems
on mipsel lately (still being worked on), bringing with it another group
of packages.  Finally, fixes have gone into britney to better support
contrib packages that have dependencies on non-existent packages, with
the result that many Java packages have also now made it into testing at
long last.

Removing these areas of blockage has significantly reduced the
release-critical bug count for sarge, which started the year at roughly
200 bugs and had dropped to around 140 bugs within a week.


This was followed immediately by the first bug-squashing party of the
year, from January 7-9, which was an unqualified success.  With an
official bug closure count of 33, and an official bug opening count of
only 2 :), the BSP has helped bring the RC bug count down below 120,
where it has been hovering for the past two weeks -- an all time low
since we started tracking RC bugs for sarge.

By this metric, we are in good shape for the release, as we're already
at our target bug count for sending out the last call for low-priority
uploads[1].  Still, extrapolating from the bug fix rate over the past
months would leave us with RC bugs for sarge open in May; hopefully the
rate will increase once the freeze begins, but in the meantime, it's
never too soon to help out with bugs on packages you care about.  If
you are interested in helping, but don't have experience with
bugsquashing, there is a new primer available at [2] that you may find
helpful.


Kernel updates
--------------

One noteworthy source of open release-critical bugs are the kernel
packages.  There have been a number of security vulnerabilities
identified in the kernel over the past few weeks, including one that has
required a change to core kernel interfaces and has therefore caused
some delays in getting fixed kernels into testing for all architectures.
At this point, fixed kernels are available on select architectures for
the first round of security vulnerabilities, but another set of
kernel-source packages are on their way.

Related to this, the decision has been made by the kernel team to drop
2.4 kernel support for hppa before the sarge release, owing to a total
lack of upstream support for this kernel branch.  Bdale Garbee has made
good progress on preparing for hppa/2.6 in d-i RC3, in spite of the fact
that d-i RC2 did not have support for it at all.

A rebuild of debian-installer kernel packages will follow as the
necessary kernels become available.  This is the main blocker for d-i
RC3, which otherwise promises to be the last revision of the installer
needed before the sarge release.


Status of security bugs in testing
----------------------------------

Outside of the numerous kernel rebuilds required, sarge seems to be in
good shape security wise:  Joey Hess has been tracking release-critical
security issues for testing, with assistance from both the Security Team
and the new Debian testing security team, and a running account of known
security vulnerabilities in testing can now be found at [3].  The count
naturally varies from day to day, but seems to have been holding between
20 and 40 for the past week.

Many of these security bugs have been fixed in unstable, and are just
waiting to propagate to testing -- largely blocked by missing builds for
the mipsel architecture.  This architecture has been hard pressed to
keep up with the package volume over the past few months.  In the long
term, additional build power is clearly needed for this architecture;
but in the short term, I would encourage maintainers to think twice
before uploading fixes for non-release critical bugs, to give some of
these higher-priority builds a chance to complete.


testing-proposed-updates, testing-security
------------------------------------------

Also in the long term, the anticipated prioritization of build queues
for testing-proposed-updates and testing-security will give us a better
mechanism for making sure important fixes get built and into testing.
Which of course bring us to the question on everyone's mind, the status
of these queues.

I'm happy to report that steady progress has been made on deploying the
necessary architecture to support testing-security (and stable-security)
for sarge.  "Steady", of course, means "not as fast as we might have
hoped", but them's the breaks.  The status today is that changes have
been completed to the dak code that will ensure the condition
stable <= proposed-updates <= testing <= testing-proposed-updates <= unstable
is satisfied by security uploads when installed in the main archive,
allowing testing-security uploads to take place without unsightly
REJECTs and manual hammering.  Some further tweaks are being made to
optimize the build daemons' interface to wanna-build, after which there
should only be the routine maintenance matter of setting up the
wanna-build databases for these queues and configuring/updating the
testing chroots on the buildds.

In short, this means that the number one blocker on the release is close
to resolution, and the testing-security and testing-proposed-updates
queues will both be fully operational in the foreseeable future.  We
should now be focusing as much as possible on stabilizing the existing
packages in the archive, to cut down on the number of RC bugs we'll have
to find and fix after the freeze starts.


other issues
------------

We are currently in the process of reducing the number of libdb-versions
in base. db4.0 has already been removed from sarge. It is currently
planned to lower the priority of db4.1; switching packages to the newer
db4.2 or db4.3 is still appreciated, as this might give us the
possiblity to also remove db4.1 later on.


upload targets
--------------

Those changes that are still needed for sarge should continue to be
uploaded according to the following guidelines:

  - If your package is frozen, but the version in unstable contains no
    changes unsuitable for testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.

  - If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.

  - If you need to do a bug fix directly in sarge, you will need to
    upload to testing-proposed-updates as well, with the same
    requirements as for frozen packages.

  - All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.

If you want to increase the urgency of an already uploaded package,
please speak with the release team; this can be sorted out without the
need for another upload.



Cheers,
-- 
NN
Debian Release Team

[1] http://lists.debian.org/debian-devel-announce/2004/11/msg00003.html
[2] http://people.debian.org/~vorlon/rc-bugsquashing.html
[3] http://merkel.debian.org/~joeyh/testing-security.html
