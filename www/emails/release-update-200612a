DRAFT
Subject: Etch frozen!

Hi,

we just edited the generic freeze file, so that all packages now need to be
hand-approved in order to go to testing.

Wait, that didn't come out quite right.  Let's try again.

  Etch is now frozen!   Wheeeeeee!!!


Thanks are due to everyone who has helped get us to this point.

For those maintainers whose packages were unprepared for a freeze at
this moment (the process has, after all, been a long one), you still
have one last opportunity to get things into shape if there are any
remaining important problems. Read on.


Now to explain what, exactly, we mean by "freeze".  The freeze
upload policy of uploading changes in through unstable if possible will
be continued to apply until the release.

This means that, for all packages that still need to be updated for
Etch, the rules are as follows:

  - If your package needs to be updated for Etch, and the version in
    unstable doesn't contain extraneous changes (e.g, the version is the
    same between testing and unstable), please upload your fix to
    unstable and contact debian-release@lists.debian.org.

  - If the version in unstable already includes significant changes not
    related to the bug to be fixed, contact debian-release about
    uploading to testing-proposed-updates.  Changed dependencies, new
    upstream versions, changed library names, and completely rewriting
    the packaging are "significant changes".  So are lots of other
    things.

  - If the version in unstable won't reach testing because of new
    library dependencies, contact debian-release about uploading to
    testing-proposed-updates.

  - If in doubt, contact debian-release first.

  - In all cases, when preparing an upload please do not make changes to
    the package that are not related to fixing the bugs in question.
    Doing so makes it more time consuming for the release team to review
    and approve such requests, delaying the release.  It also delays the
    fix for your package, because you will be asked to reupload.

  - When contacting the release team, please explain why you are
    requesting an update.  Bug numbers are a must.  The more we can
    figure out from your first email and your changelog (if any), the
    more quickly we can get your update in.

  - If you have a package that needs updating, *please* don't forget to
    contact us.  *Don't expect us to find out about it on our own*.
    Putting a comment in the changelog is not contacting the release
    team. :)

  - If your package has been removed recently (i.e. in the last 20 days)
    due to an RC bug, and you have an bugfix-only update uploaded,
    you can contact the release team about letting your package back in.
    Same as above: Do not expect us to find it out ourself. You need to
    push that.


Now, so as not to have everyone contact us at once about packages we
know we won't approve, here are the guidelines for changes that will be
accepted into testing during the freeze:

  - fixes for release critical bugs (i.e., bugs of severity critical,
    grave, and serious) in all packages;

  - changes for release goals, if they are not invasive;

  - fixes for severity: important bugs in packages of priority: optional
    or extra, only when this can be done via unstable;

  - translation updates and

  - documentation fixes.



As always, it is the release team's goal to get as much good software
into Etch as possible.  However, a freeze does not mean that your
package is ensured a spot in the release.  Please continue to stay on
top of release-critical bugs in packages that you maintain; RC bugs in
optional or extra packages that remain unfixed after a week will still
be grounds for removal from testing, just as they have been up to this
point.

Please also note that since many updates (hopefully, the vast majority)
will still be going in through unstable, major changes in unstable right
now can disrupt efforts to get RC bugs fixed.  We don't ask you not to
make changes in unstable, but we do ask that you be aware of the effects
your changes can have -- especially if you maintain a library -- and to
feel free to continue making use of experimental where appropriate.
Note once again that you can stage NEW uploads in experimental to avoid
disruption in unstable.

Also, in case you need release team's help to fix RC bugs (e.g. to remove an
old package), please feel free to contact us.

For packages which missed the freeze only for reasons outside of the
control of the maintainers, we might be generous, but you to contact us on
your own, and you need to contact us soon.


What needs to happen before release
===================================
There is a short list of things that need to happen, though.

Fixing of release critical bugs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
For the release, we need to get rid of all release critical bugs. Please
don't hesitate, pick any bug from
http://bts.turmzimmer.net/details.php?bydist=etch and fix it. Or send in a
patch in case there is none yet. And of course, follow our permanent BSP
policy for your NMUs. Uploading works as you are used to -- just remember to
send an e-mail to debian-release@lists.debian.org to get your fix through.
              

Security support
~~~~~~~~~~~~~~~~
Final preparations for security support will be done during the general
freeze, that is now. We hope being able to announce start of security
support soon, but obviously, it is a pre-condition for release.




Cheers,
NN
-- 
Debian Release Team
http://release.debian.org/
